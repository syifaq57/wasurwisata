/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  ImageBackground,
} from 'react-native';

import {Container, Text, Content} from 'native-base';

import {useRoute} from '@react-navigation/native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors/index';
import DeafaultHeader from '../../component/DeafaultHeader';

const DetailUser = () => {
  const route = useRoute();

  const [state, setState] = useState({
    nama: '',
    loading: false,
    form: {
      judul: '',
      deskripsi: '',
      foto: '',
    },
  });

  const updateLocalState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const updateFormState = (newData) => {
    updateLocalState({
      form: {
        ...state.form,
        ...newData,
      },
    });
  };

  const checkParams = async () => {
    await updateLocalState({loading: true});

    const wisata = (await route.params) ? route.params.data : null;

    if (wisata !== null) {
      updateFormState({
        judul: wisata.judul,
        deskripsi: wisata.deskripsi,
        foto: wisata.foto,
      });
    }

    await updateLocalState({loading: false});
  };

  useEffect(() => {
    checkParams();
  }, []);

  return (
    <Container
      style={{
        flex: 1,
        backgroundColor: colors.white,
      }}>
      <Image
        source={require('../../res/image/background.jpg')}
        resizeMode="contain"
        style={{position: 'absolute'}}
      />
      <ImageBackground
        style={{flex: 1}}
        // source={require('../../res/image/BGdashboard.jpg')}
        // source={require('../../res/image/background.jpg')}
      >
        <DeafaultHeader
          backgroundColor={colors.primarydark}
          backButton
          title="Detail Wisata"
        />
        <View style={{marginBottom: wp(2), alignItems: 'center'}}>
          <TouchableOpacity>
            <Image
              source={{
                uri: state.form?.foto,
              }}
              style={{
                height: wp(60),
                width: wp(100),
                // borderRadius: hp(20),
                resizeMode: 'cover',
              }}
            />
          </TouchableOpacity>
        </View>
        <Content>
          {state.loading ? (
            <View style={{paddingVertical: hp('2%')}}>
              <ActivityIndicator size={'large'} />
            </View>
          ) : (
            <View>
              {/* <View
                style={{
                  paddingHorizontal: wp(5),
                  marginLeft: wp(2),
                  marginTop: hp(-1),
                }}>
                <Text style={{fontSize: wp(5), fontWeight: 'bold'}}>Judul</Text>
              </View> */}
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: wp(2),
                  marginHorizontal: wp(3),
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    // marginLeft: wp(2),
                    fontSize: wp(4.5),
                    fontWeight: 'bold',
                    color: colors.green02,
                  }}>
                  {state.form.judul}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: wp(2),
                  marginHorizontal: wp(3),
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    // marginLeft: wp(2),
                    fontSize: wp(4),
                    // fontWeight: 'bold',
                    color: colors.black,
                  }}>
                  {state.form?.deskripsi}
                </Text>
              </View>
            </View>
          )}
        </Content>
      </ImageBackground>
    </Container>
  );
};

export default DetailUser;
