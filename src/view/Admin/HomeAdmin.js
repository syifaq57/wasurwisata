/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  AsyncStorage,
  ImageBackground,
  BackHandler,
  Alert,
} from 'react-native';

import {Container, Text} from 'native-base';

import {useNavigation, useFocusEffect} from '@react-navigation/native';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors/index';
import LinearGradient from 'react-native-linear-gradient';
import DeafaultHeader from '../../component/DeafaultHeader';

const HomeAdmin = () => {
  const navigation = useNavigation();
  const [state, setState] = useState({
    nama: '',
    antrian: '-',
  });

  const updateLocalState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const getAntrian = () => {
    AsyncStorage.getItem('antrian').then((item) => {
      if (item) {
        console.log('get antri', item);
        updateLocalState({
          antrian: item,
        });
      }
    });
  };

  useEffect(() => {
    AsyncStorage.getItem('userData').then((item) => {
      const user = JSON.parse(item);
      if (user && Object.keys(user).length > 0) {
        updateLocalState({
          nama: user.nama,
        });
      }
      // console.log('parse', parse);
    });
    getAntrian();
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        Alert.alert('Exit App!', 'Apakah Kamu yakin ingin keluar Aplikasi?', [
          {
            text: 'Cancel',
            onPress: () => {
              // return true;
            },
            style: 'cancel',
          },
          {
            text: 'YES',
            onPress: () => {
              BackHandler.exitApp();
              // return false;
            },
          },
        ]);
        return true;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, []),
  );

  return (
    <Container
      style={{
        flex: 1,
        backgroundColor: colors.white,
      }}>
      <Image
        source={require('../../res/image/background.jpg')}
        resizeMode="contain"
        style={{position: 'absolute'}}
      />
      <ImageBackground
        style={{flex: 1, paddingTop: hp(1.3)}}
        // source={require('../../res/image/BGdashboard.jpg')}
        // source={require('../../res/image/background.jpg')}
      >
        <DeafaultHeader
          title={`Hi, ${state.nama}`}
          rightButton="user"
          righButtonSize={hp(4)}
          onRightButtonPress={() => {
            navigation.navigate('ProfileScreen');
          }}
        />
        <View style={{paddingHorizontal: wp(5), marginTop: hp(-1)}}>
          <View style={{marginTop: wp(0)}}>
            <Text style={{fontSize: wp(4.2)}}>Berikut Jumlah Pengunjung</Text>
          </View>
          <TouchableOpacity onPress={() => {
              navigation.navigate('UserList', {today: true});
            }}>
            <LinearGradient
              // start={{x: 0, y: 0}}
              // end={{x: 1, y: 0}}
              colors={[colors.green02, colors.primary]}
              style={{
                flexDirection: 'row',
                marginVertical: hp(2.6),
                borderRadius: wp(2),
                height: hp(15),
                alignItems: 'center',
                paddingHorizontal: wp(4),
              }}>
              <View style={{flex: 1.5}}>
                <View
                  style={{
                    // backgroundColor: colors.white,
                    height: wp(20),
                    width: wp(20),
                    borderRadius: wp(20),
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    style={{
                      width: wp(15),
                      height: wp(15),
                      resizeMode: 'contain',
                    }}
                    source={require('../../res/image/traveler.png')}
                  />
                </View>
              </View>

              <View style={{flex: 4}}>
                <Text
                  style={{
                    fontSize: wp(5),
                    fontWeight: 'bold',
                    color: colors.lightBlack,
                  }}>
                  Jumlah Pengunjung
                </Text>
                <Text
                  style={{
                    marginLeft: wp(2),
                    marginTop: wp(2.6),
                    fontSize: wp(5.6),
                    fontWeight: 'bold',
                    color: colors.black,
                  }}>
                  {state.antrian}
                </Text>
              </View>
            </LinearGradient>
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              marginVertical: wp(2),
              marginLeft: wp(2),
            }}>
            <FontAwesome5
              name="tools"
              color={colors.primarydark}
              size={wp(6)}
            />
            <Text
              style={{
                marginLeft: wp(2),
                fontSize: wp(5),
                fontWeight: 'bold',
                color: colors.lightBlack,
              }}>
              Menu
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('TambahFasilitas');
            }}>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              colors={['rgba(18, 145, 136, 0.5)', 'rgba(255, 255, 255, 0.3)']}
              style={{
                flexDirection: 'row',
                marginVertical: hp(1),
                borderRadius: wp(2),
                height: hp(15),
                alignItems: 'center',
                paddingHorizontal: wp(4),
              }}>
              <Image
                style={{
                  flex: 1,
                  width: wp(15),
                  height: wp(15),
                  resizeMode: 'contain',
                }}
                source={require('../../res/image/park.png')}
              />
              <Text
                style={{
                  flex: 3,
                  marginLeft: wp(2),
                  fontSize: wp(5),
                  fontWeight: 'bold',
                  color: colors.lightBlack,
                }}>
                Tambah Fasilitas
              </Text>
              <FontAwesome
                style={{flex: 0.4}}
                name="gear"
                color={colors.black}
                size={wp(6)}
              />
            </LinearGradient>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('UserList');
            }}>
            <LinearGradient
              start={{x: 1, y: 0}}
              end={{x: 0, y: 0}}
              // colors={['#dbb542', '#fdff63']}
              colors={['rgba(18, 145, 136, 0.7)', 'rgba(255, 255, 255, 0.3)']}
              style={{
                flexDirection: 'row',
                marginVertical: hp(1),
                borderRadius: wp(2),
                height: hp(15),
                alignItems: 'center',
                paddingHorizontal: wp(4),
              }}>
              <FontAwesome
                style={{flex: 0.4}}
                name="gear"
                color={colors.black}
                size={wp(6)}
              />
              <Text
                style={{
                  flex: 3,
                  marginLeft: wp(2),
                  fontSize: wp(5),
                  fontWeight: 'bold',
                  color: colors.lightBlack,
                  textAlign: 'right',
                  marginRight: wp(3.5),
                }}>
                Daftar Pengguna
              </Text>
              <Image
                style={{
                  flex: 1,
                  width: wp(15),
                  height: wp(15),
                  resizeMode: 'contain',
                }}
                source={require('../../res/image/teamwork.png')}
              />
            </LinearGradient>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              navigation.navigate('ReportPage');
            }}>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              colors={['rgba(18, 145, 136, 0.5)', 'rgba(255, 255, 255, 0.3)']}
              style={{
                flexDirection: 'row',
                marginVertical: hp(1),
                borderRadius: wp(2),
                height: hp(15),
                alignItems: 'center',
                paddingHorizontal: wp(4),
              }}>
              <Image
                style={{
                  flex: 1,
                  width: wp(15),
                  height: wp(15),
                  resizeMode: 'contain',
                }}
                source={require('../../res/image/report.png')}
              />
              <Text
                style={{
                  flex: 3,
                  marginLeft: wp(2),
                  fontSize: wp(5),
                  fontWeight: 'bold',
                  color: colors.lightBlack,
                }}>
                {'Laporan\nData Pengunjung'}
              </Text>
              <FontAwesome
                style={{flex: 0.4}}
                name="gear"
                color={colors.black}
                size={wp(6)}
              />
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </Container>
  );
};

export default HomeAdmin;
