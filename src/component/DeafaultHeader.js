/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, TextInput, TouchableOpacity} from 'react-native';
import {Text, Header} from 'native-base';
import colors from '../res/colors/index';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import Fontisto from 'react-native-vector-icons/Fontisto';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';

const DeafaultHeader = (props) => {
  const navigation = useNavigation();
  return (
    <Header
      transparent
      style={{
        backgroundColor: props.backgroundColor,
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          flexDirection: 'row',
          paddingLeft: props.backgroundColor ? wp(1) : wp(2.2),
          paddingRight: wp(2),
        }}>
        {props.backButton ? (
          <View style={{flex: 0.7}}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}>
              <FontAweSome
                name="arrow-left"
                color={props.backgroundColor ? colors.white : colors.green02}
                size={hp(3)}
              />
            </TouchableOpacity>
          </View>
        ) : null}
        {props.leftIcon ? (
          <View style={{flex: 0.7}}>
            <TouchableOpacity onPress={props.pickImage}>
              <Fontisto name="camera" color={colors.green02} size={hp(3)} />
            </TouchableOpacity>
          </View>
        ) : null}
        <Text
          style={{
            textAlign: props.centerTitle ? 'center' : 'left',
            flex: 6,
            fontSize: hp(3),
            color: props.backgroundColor ? colors.white : colors.green02,
            fontWeight: 'bold',
          }}>
          {props.title}
        </Text>
        {/* {props.aboutButton ? (
          <View style={{flex: 0.5, alignItems: 'center'}}>
            <TouchableOpacity
              onPress={() => props.showSideBar()}
              style={{flex: 1, justifyContent: 'center'}}
              transparent>
              <Icon
                name="info"
                size={hp('4%')}
                style={{color: 'white', marginTop: 2}}
              />
            </TouchableOpacity>
          </View>
        ) : null} */}
        {props.rightButton ? (
          <View style={{flex: 0.5, alignItems: 'center'}}>
            <TouchableOpacity
              onPress={props.onRightButtonPress}
              style={{flex: 1, justifyContent: 'center'}}
              transparent>
              <FontAweSome
                name={
                  props.rightButton.length > 0 ? props.rightButton : 'pencil'
                }
                color={colors.green02}
                size={props.righButtonSize ? props.righButtonSize : hp(3)}
              />
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    </Header>
  );
};

export default DeafaultHeader;
