/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, ActivityIndicator} from 'react-native';
import Modal from 'react-native-modal';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const ReLoadingModal = (props) => {
  return (
    <View>
      <Modal
        style={{ alignItems: 'center' }}
        backdropOpacity={0.2}
        animationIn={'fadeIn'}
        animationOut={'fadeOut'}
        useNativeDriver={true}
        isVisible={props.visible}>
        <View
          style={{
            backgroundColor: 'white',
            borderRadius: wp('2%'),
            padding: hp('1%'),
            width: wp('50%'),
          }}>
          <View
            style={{
              paddingBottom: hp('1%'),
              borderBottomWidth: 0.5,
              borderColor: 'gray',
              alignItems: 'center',
            }}>
            <Text style={{ fontSize: wp('3%'), fontWeight: 'bold' }}>
              {props.title ? props.title : 'Loading ..'}
            </Text>
          </View>
          <View style={{ paddingVertical: hp('2%') }}>
            <ActivityIndicator size={'large'} color="black" />
          </View>
        </View>
      </Modal>
    </View>

  );
};

export default ReLoadingModal;
