/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Image} from 'react-native';
import {Container, Content, Text, Button} from 'native-base';
import Modal from 'react-native-modal';

import colors from '../res/colors/index';

import ImagePicker from 'react-native-image-crop-picker';

// props
// *
// visible
// onCancel
// pickCamera
// pickGalery

const ReModalImagePicker = (props) => {
  const pickGalery = (cropping, mediaType = 'photo') => {
    ImagePicker.openPicker({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then((image) => {
        const result = {
          name: image.modificationDate,
          exif: image.exif,
          uri: image.path,
          width: image.width,
          height: image.height,
          mime: image.mime,
        };
        props.onPickImage(result);
      })
      .catch((e) => {
        console.log(e);
        // Alert.alert(e.message ? e.message : e);
      });
  };

  const pickCamera = (cropping, mediaType = 'photo') => {
    ImagePicker.openCamera({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then((image) => {
        const result = {
          name: image.modificationDate,
          exif: image.exif,
          uri: image.path,
          width: image.width,
          height: image.height,
          mime: image.mime,
        };
        props.onPickImage(result);
      })
      .catch((e) => {
        console.log(e);
        // Alert.alert(e.message ? e.message : e);
      });
  };

  return (
    <View>
      <Modal
        style={{justifyContent: 'flex-end'}}
        backdropOpacity={0.3}
        animationIn={'slideInUp'}
        animationOut={'slideOutDown'}
        isVisible={props.visible}
        onBackdropPress={props.onCancel}
        onBackButtonPress={props.onCancel}>
        <View style={{marginBottom: 10}}>
          <View
            style={{
              borderRadius: 5,
              flexDirection: 'row',
              paddingHorizontal: 30,
              paddingVertical: 40,
              backgroundColor: 'white',
            }}>
            <View style={{marginRight: 10, flex: 1}}>
              <Button
                transparent
                onPress={() => {
                  pickCamera();
                }}>
                <View style={{alignItems: 'center'}}>
                  <Image
                    source={require('../res/image/camera.png')}
                    style={{
                      height: 70,
                      width: 90,
                      resizeMode: 'contain',
                    }}
                  />
                  <View style={{width: '100%'}}>
                    <Text
                      style={{
                        fontSize: 12,
                        color: colors.greenpln,
                      }}>
                      Take a Photo
                    </Text>
                  </View>
                </View>
              </Button>
            </View>
            <View style={{marginLeft: 10, flex: 1}}>
              <Button
                transparent
                onPress={() => {
                  pickGalery();
                }}>
                <View style={{alignItems: 'center'}}>
                  <Image
                    source={require('../res/image/galery.png')}
                    style={{
                      height: 70,
                      width: 90,
                      resizeMode: 'contain',
                    }}
                  />
                  <View style={{width: '100%'}}>
                    <Text
                      style={{
                        fontSize: 12,
                        color: colors.greenpln,
                      }}>
                      Select from Galery
                    </Text>
                  </View>
                </View>
              </Button>
            </View>
          </View>
        </View>

        <View style={{borderRadius: 5, marginBottom: 10}}>
          <Button onPress={props.onCancel} style={{backgroundColor: 'white'}}>
            <View style={{flex: 1}}>
              <Text
                style={{
                  color: colors.greenpln,
                  textAlign: 'center',
                }}>
                Cancel
              </Text>
            </View>
          </Button>
        </View>
      </Modal>
    </View>
  );
};

export default ReModalImagePicker;
