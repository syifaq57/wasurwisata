/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../res/colors';

const ReAlertModal = (props) => {
  return (
    <View>
      <Modal
        style={{ alignItems: 'center' }}
        backdropOpacity={0.2}
        animationIn={'fadeIn'}
        animationOut={'fadeOut'}
        isVisible={props.visible}>
        <View
          style={{
            backgroundColor: 'white',
            borderRadius: wp('2%'),
            padding: hp('1%'),
            paddingBottom: wp(2),
            width: wp('50%'),
          }}>
          <View
            style={{
              paddingBottom: hp('1%'),
              borderBottomWidth: 0.5,
              borderColor: 'gray',
              alignItems: 'center',
            }}>
            <Text style={{ fontSize: wp('3%'), fontWeight: 'bold' }}>
              {props.title ? props.title : 'Berhasil'}
            </Text>
          </View>
          <View
            style={{
              paddingVertical: hp('2%'),
              alignItems: 'center',
            }}>
            <Text style={{ fontSize: wp('3%') }}>
              {props.subTitle ? props.subTitle : ''}
            </Text>
          </View>
          <View style={{ alignItems: 'center' }}>
            <TouchableOpacity
              style={{
                borderRadius: wp(1),
                alignItems: 'center',
                justifyContent: 'center',
                width: wp(16),
                height: wp(8),
                // borderWidth: 1,
                backgroundColor: colors.whiteBlue,
              }}
              onPress={props.onConfirm}>
              <Text
                style={{
                  fontSize: wp('3%'),
                  color: colors.white,
                  fontWeight: 'bold',
                }}>
                Ok
            </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>

  );
};

export default ReAlertModal;
