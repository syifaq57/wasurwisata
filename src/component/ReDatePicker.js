import React, { useState } from 'react';
import {View, Text} from 'react-native';
import colors from '../res/colors/index';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import DatePicker from 'react-native-datepicker';

const ReDatePicker = (props) => {
  const [state, setState] = useState({
    date: '',
  });
  const labelStyle = {
    position: 'absolute',
    top: hp(0.1),
    fontSize: wp(2.8),
    color: colors.lightBlack,
    fontFamily: 'Poppins-Regular',
  };
  return (
    <View
      style={{
        borderBottomWidth: 2,
        borderColor: colors.black,
      }}>
      <DatePicker
        style={{width: wp(90), paddingBottom: wp(2)}}
        date={props.selectedDate}
        mode="date"
        placeholder={props.placeholder ? props.placeholder : 'Select Date'}
        format="YYYY-MM-DD"
        // minDate="2019-01-01"
        // maxDate="2030-06-01"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateIcon: {
            position: 'absolute',
            right: 0,
            // top: 7,
            marginLeft: 0,
          },
          dateInput: {
            borderWidth: 0,
          },
          dateText: {
            alignSelf: 'flex-start',
            marginLeft: wp(1),
            fontSize: wp(4),
            color: colors.green02,
          },
          dateTouchBody: {
            // borderColor: colors.gray05,
            // borderBottomWidth: 1,
            borderRadius: 5,
            minHeight: hp(7.8),
          },
          placeholderText: {
            alignSelf: 'flex-start',
            marginLeft: wp(1),
            fontSize: wp(4),
            color: colors.gray10,
          },
          // ... You can check the source to find the other keys.
        }}
        onDateChange={(date) => {
          props.onDateChange(date);
        }}
      />
      {props.selectedDate && props.selectedDate.length > 0 ? (
        <Text style={[labelStyle]}> {props.label} </Text>
      ) : null}
    </View>
  );
};

export default ReDatePicker;
